"use strict";
class InMemoryDataService {
    createDb() {
        let contatos = [
            { id: 1, nome: "Jardier", email: "jardier@hotmail.com", telefone: "(85) 99621 8413" },
            { id: 2, nome: "Aires", email: "aires@hotmail.com", telefone: "(85) 99621 8513" },
            { id: 3, nome: "Bezerra", email: "bezerra@hotmail.com", telefone: "(85) 99621 8413" },
            { id: 4, nome: "Bárbara", email: "barbara@hotmail.com", telefone: "(85) 99621 8413" },
            { id: 5, nome: "Adriana", email: "adriana@hotmail.com", telefone: "(85) 99621 8413" },
        ];
        return { contatos };
    }
}
exports.InMemoryDataService = InMemoryDataService;
//# sourceMappingURL=in-memory-data.service.js.map