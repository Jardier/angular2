import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const appsRoutes : Routes = [
    {
        path : '',
        redirectTo: '/contato',
        pathMatch: 'full'
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(appsRoutes)
    ], 
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule{

}