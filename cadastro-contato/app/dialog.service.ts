import { Injectable } from '@angular/core';

@Injectable()
export class DialogService {

    public confirma(mensagem?:string) {
        return new Promise(resolve => {
            return resolve(window.confirm(mensagem || 'Confima?'));
        });
    }
}