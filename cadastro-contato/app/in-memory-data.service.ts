import { InMemoryDbService } from 'angular-in-memory-web-api';

import { Contato } from './contatos/contato.model';

export class InMemoryDataService implements InMemoryDbService {

    public createDb(): {} {

        let contatos : Contato [] = [
            {id : 1, nome: "Jardier", email: "jardier@hotmail.com", telefone: "(85) 99621 8413"},
            {id : 2, nome: "Aires", email: "aires@hotmail.com", telefone: "(85) 99621 8513"},
            {id : 3, nome: "Bezerra", email: "bezerra@hotmail.com", telefone: "(85) 99621 8413"},
            {id : 4, nome: "Bárbara", email: "barbara@hotmail.com", telefone: "(85) 99621 8413"},
            {id : 5, nome: "Adriana", email: "adriana@hotmail.com", telefone: "(85) 99621 8413"},
        ];
        return {contatos};
    }
}