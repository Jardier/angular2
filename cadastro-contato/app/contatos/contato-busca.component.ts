import { Component, OnInit, Input, Output, OnChanges, SimpleChange,
         SimpleChanges, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';


import { Contato } from './contato.model';
import { ContatoService } from './contato.service';


@Component({
    moduleId : module.id,
    selector: 'contato-busca',
    templateUrl: 'contato-busca.component.html',
    styles : [`
        .cursor-pointer:hover {
            cursor : pointer;
        }
    `]
})

export class ContatoBuscaComponent implements OnInit, OnChanges {
    @Input()
    private busca : string;
    @Output()
    private buscaChange: EventEmitter<string> = new EventEmitter<string>();

    private contatos : Observable<Contato[]>;
    private termosDaBusca : Subject<any> = new Subject<any>();

    constructor(
         private contatoService : ContatoService,
         private router : Router
    ) { }

    ngOnInit() : void {
        this.contatos = this.termosDaBusca
            .debounceTime(500) //aguarda 500ms para emitir nocos eventos de busca
            .distinctUntilChanged() //ignore se o próximo termo da busca for igual ao anterior
            .switchMap(termo => termo ? this.contatoService.search(termo) : Observable.of<Contato[]>([]))
            .catch(error => Observable.of<Contato[]>([]));

     }
    
     ngOnChanges(changes : SimpleChanges) : void {
        let busca : SimpleChange = changes['busca'];
        this.search(busca.currentValue);        
     }

    public search(termo : string) : void {
        this.termosDaBusca.next(termo);
        this.buscaChange.emit(termo);
    }

    public verDetalhe(contato : Contato) : void {
        let link = `/contato/save/${contato.id}`;
        this.router.navigateByUrl(link);
        this.buscaChange.emit('');
    }
}