import { Component, OnInit } from '@angular/core';
import { Contato } from './contato.model';
import { ContatoService } from './contato.service';
import { DialogService } from '../dialog.service';

@Component({
    moduleId: module.id,
    selector: 'contatos-lista',
    templateUrl: 'contatos-lista.component.html',    
})
export class ContatosListaComponent {

    private contatos : Contato [] = [];
    private mensagem : {};
    private classeCss : {};
    private currentTimeout : any;


    constructor(
        private contatoService: ContatoService,
        private dialogService : DialogService) {}

    ngOnInit(): void {
        this.contatoService.findAll()
            .then((contatos: Contato[]) => {
              this.contatos = contatos;  
            }).catch(err => {
                console.log(err);
                this.mostrarMensagem({
                    tipo: 'danger',
                    texto: 'Ocorreu um erro ao carregar a lista de contatos'
                });
            });
    }

    public onDelete(contato : Contato) : void {
       this.dialogService.confirma('Deseja deletar o Contato: ' + contato.nome + '?')
            .then((canDelete : boolean) => {
                if(canDelete) {
                    this.contatoService.delete(contato)
                    .then(() => {
                        this.contatos = this.contatos.filter((c : Contato) => c.id != contato.id);
                        this.mostrarMensagem({tipo: 'success', texto: 'Contato deletado com sucesso!'});
                    }).catch(err =>{
                        console.error(err);
                        this.mostrarMensagem(
                            { tipo: 'danger', 
                              texto: 'Ocorreu um erro ao deletar o contato: ' + contato.nome}
                            );
                    })
                }
            })
       //this.contatoService.delete(this.contato).then( contato => this.location.back());
   }

   private mostrarMensagem(mensagem : {tipo : string, texto : string}) : void {
        this.mensagem = mensagem;
        this.montarClasseCss(mensagem.tipo);

        if(mensagem.tipo != 'danger') {
            if(this.currentTimeout) {
                clearTimeout(this.currentTimeout);
            }
            this.currentTimeout = setTimeout(() => {
                this.mensagem = undefined
            }, 3000);
        }
       
   }
    private montarClasseCss(tipo : string) : void {
        this.classeCss = {
            'alert' : true
        };
        this.classeCss ['alert-' + tipo] = true;
    }
}