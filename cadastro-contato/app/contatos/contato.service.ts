import { Injectable } from '@angular/core';
import { Contato } from './contato.model';
import { Observable } from 'rxjs';

import { ServiceInterface } from './../interfaces/service.interface';

import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class ContatoService implements ServiceInterface {
    //retorno do metódo createDB, com o recurso
    private contatosUrl : string = 'app/contatos'; 
    private headers : Headers = new Headers({'Content-Type': 'application/json'});
    
    constructor(
        private http : Http,
    ) {}

    public findAll() : Promise<Contato[]> {
        return this.http.get(this.contatosUrl)
            .toPromise()
            .then(response => response.json().data as Contato[])
            .catch(this.handleError);

    }

    public find(id : number) : Promise<Contato> {
        return this.findAll()
            .then((contatos: Contato[]) => contatos.find(contato => contato.id === id));
    }

    public create(contato : Contato) : Promise<Contato> {
        return this.http
            .post(this.contatosUrl, JSON.stringify(contato), {headers : this.headers})
            .toPromise()
            .then((response : Response ) => response.json().data as Contato)
            .catch(this.handleError);
    }

    public update(contato : Contato) : Promise<Contato> {
        const url = `${this.contatosUrl}/${contato.id}`;        
        return this.http
            .put(url, JSON.stringify(contato), {headers : this.headers})
            .toPromise()
            .then(() => contato as Contato)
            .catch(this.handleError);
    }

    public delete(contato : Contato) : Promise<Contato> {
        const url = `${this.contatosUrl}/${contato.id}`;
        return this.http
            .delete(url, {headers : this.headers})
            .toPromise()
            .then(() => contato as Contato)
            .catch(this.handleError);
    }

    private handleError(err : any) : any {
        console.log(err);
        return Promise.reject(err.message || err);
    }

    getContatosSlowly() : Promise<Contato[]> {
        return new Promise((resolve, reject) => {
            setTimeout(resolve, 3000);
        })
        .then(() => {
            console.log("Primeiro Then");
            return "Curso de Angular 2 com TypeScripty"
        })
        .then((param : string) => {
            console.log("Segundo Then");           
            console.info("Exibindo informações do parâmetro, do then anterior... " + param);

            return new Promise((resolve2, reject2) => {
                setTimeout(() => {
                    console.warn("Continuando depois de 4 segundos ...");
                    resolve2();
                },4000);
            })
        })
        .then(() => {
            console.log("Terceiro Then");
            return this.getContatos();
        });              
    }

    public search(termo : string) : Observable<Contato[]> {
        return this.http
            .get(`${this.contatosUrl}/?nome=${termo}`)
            .map((res : Response) => res.json().data as Contato[]);
    }
}