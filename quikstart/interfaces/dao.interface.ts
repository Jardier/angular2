export interface DaoInterface {

    tableName : string;

    inser(object : any) : boolean;
    update(object : any) : boolean;
    delete(id : number) : any;
    find(id: number) : any;
    findAll() : [any];

}