import {DaoInterface} from './dao.interface';
import {Animal} from './../classes/animal';

export class AnimalDao implements DaoInterface{
    
    tableName : string =  '';
    
    inser(object: Animal): boolean {
        console.log('insert ...');
        object.mover(50);
        return true;
    }
    update(object: Animal): boolean {
        return true;
    }
    delete(id: number) : Animal {
        return null;
    }
    find(id: number) : Animal {
       return null;
    }
    findAll(): [Animal] {
        return [new Animal("Rex")];
    }

    
}