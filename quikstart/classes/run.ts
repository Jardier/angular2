import {Animal} from './animal';
import {Cavalo} from './cavalo';

let a = new Animal("Rex");
a.mover(50);

let b = new Cavalo("Pé de Pano");
b.mover(100);
